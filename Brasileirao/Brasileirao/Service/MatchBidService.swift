//
//  MatchBidService.swift
//  Brasileirao
//
//  Created by Andrew Castro on 24/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation
import Alamofire

final class MatchBidService {
    
    private var alamofireManager: Alamofire.SessionManager
    private var url: String = ""
    
    init(_ url: String, timeout: TimeInterval = 15) {
        self.url = url
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = timeout
        configuration.timeoutIntervalForResource = timeout
        alamofireManager = Alamofire.SessionManager(configuration:configuration)
    }
    
    func getMatchBid(completionHandler: @escaping(_ matchList: MatchDetail?, _ error: BaseError?) -> Void) {
        alamofireManager.request(self.url).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
            switch response.result {
            case .success:
                guard let jsonData = response.data, let matchDetail = try? JSONDecoder().decode(MatchDetail.self, from: jsonData) else {
                    fallthrough
                }
                completionHandler(matchDetail, nil)
            case .failure:
                var error = BaseError()
                error.title = "Ocorreu um erro inesperado"
                error.message = "Por favor, tente novamente! Estamos trabalhando para resolver isso"
                completionHandler(nil, error)
            }
        }
    }
}
