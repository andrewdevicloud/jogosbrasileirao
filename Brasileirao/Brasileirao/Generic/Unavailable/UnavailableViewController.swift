//
//  UnavailableViewController.swift
//  Brasileirao
//
//  Created by Andrew Castro on 25/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import UIKit

protocol UnavailableViewControllerDelegate {
    func refreshViewController()
}

class UnavailableViewController: UIViewController {
    
    var error: BaseError?
    var delegate: UnavailableViewControllerDelegate?
    
    @IBOutlet var titleMessage: UILabel!
    @IBOutlet var subtitleMessage: UILabel!
    
    @IBAction func refreshControl(_ sender: Any) {
        self.delegate?.refreshViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }

    private func configure() {
        if let error = self.error {
            self.titleMessage.text = error.title
            self.subtitleMessage.text = error.message
        }
    }

}
