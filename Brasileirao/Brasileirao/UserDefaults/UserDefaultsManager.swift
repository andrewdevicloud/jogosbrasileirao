//
//  UserDefaultsManager.swift
//  Brasileirao
//
//  Created by Andrew Castro on 25/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation
import UIKit

struct MatchInformationUserDefaults: Codable {
    var homeTeam = TeamInformationUserDefaults()
    var outTeam = TeamInformationUserDefaults()
    var matchDate = ""
    var hour = ""
    var local = ""
}

struct TeamInformationUserDefaults: Codable {
    var name = ""
    var shield = ""
    var goalsMadeMatch = ""
}

class UserDefaultsManager {
    
    private let userdefault = UserDefaults.standard
    
    func saveObject(matchList: [MatchInformationViewData]) {
        if !matchList.isEmpty {
            let matchListDatabase = self.parse(the: matchList)
            let enconder = JSONEncoder()
            if let encoded = try? enconder.encode(matchListDatabase) {
                self.userdefault.set(encoded, forKey: "MatchListSaveCache")
            }
        }
    }
    
    func getSaveContacts() -> [MatchInformationUserDefaults]? {
        if let data = self.userdefault.value(forKey:"MatchListSaveCache") as? Data {
            let contacts = try? JSONDecoder().decode([MatchInformationUserDefaults].self, from: data)
            return contacts ?? nil
        }
        return [MatchInformationUserDefaults]()
    }
    
    func removeBackup(){
        self.userdefault.removeObject(forKey: "MatchListSaveCache")
    }
    
    private func parse(the object: [MatchInformationViewData]) -> [MatchInformationUserDefaults] {
        var matchInformationDatabase = [MatchInformationUserDefaults]()
        for matchInformation in object {
            var matchInformationItem = MatchInformationUserDefaults()
            matchInformationItem.homeTeam = self.mapper(the: matchInformation.homeTeam)
            matchInformationItem.outTeam = self.mapper(the: matchInformation.outTeam)
            matchInformationItem.matchDate = matchInformation.matchDate
            matchInformationItem.hour = matchInformation.hour
            matchInformationItem.local = matchInformation.local
            
            matchInformationDatabase.append(matchInformationItem)
        }
        
        return matchInformationDatabase
    }
    
    private func mapper(the object: TeamInformationViewData) -> TeamInformationUserDefaults {
        var teamInformationItem = TeamInformationUserDefaults()
        teamInformationItem.name = object.name
        teamInformationItem.shield = object.shield
        teamInformationItem.goalsMadeMatch = object.goalsMadeMatch
        return teamInformationItem
    }
   
}
