//
//  MatchDetail.swift
//  Brasileirao
//
//  Created by Andrew Castro on 24/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation

struct MatchDetail: Codable {
    var matchBids: [MatchBids]?
}

struct MatchBids: Codable {
    var matchTime: Int?
    var bidDescription: String?
    var bidImportance: String?
}
