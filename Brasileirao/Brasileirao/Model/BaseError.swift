//
//  BaseError.swift
//  Brasileirao
//
//  Created by Andrew Castro on 22/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation

struct BaseError {
    var title: String?
    var message: String?
}
