//
//  MatchList.swift
//  Brasileirao
//
//  Created by Andrew Castro on 22/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation

struct MatchList: Codable {
    var round: Int?
    var matchList: [MatchInformation]?
}

struct MatchInformation: Codable {
    var id: Int?
    var homeTeam: TeamInformation?
    var outTeam: TeamInformation?
    var matchDate: String?
    var hour: String?
    var local: String?
    var matchStatus: String?
}

struct TeamInformation: Codable {
    var name: String?
    var shield: String?
    var goalsMadeMatch: Int?
}
