//
//  MatchDetailPresenter.swift
//  Brasileirao
//
//  Created by Andrew Castro on 24/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation

protocol MatchDetailPresenterDelegate {
    func takeMatchBid(_ matchDetailViewData: MatchDetailViewData)
    func error(_ error: BaseError)
}

struct MatchDetailViewData {
    var matchBids = [MatchBidsViewData]()
}

struct MatchBidsViewData {
    var matchTime = 0
    var bidDescription = ""
    var bidImportance = ""
}

class MatchDetailPresenter {
    
    var timer : Timer?
    var delegate: MatchDetailPresenterDelegate?
    
    private let service = MatchBidService("https://private-f25749-detalhesdapartida.apiary-mock.com/matchList/1")
    
    func checkProgressMatch(matchListInformation: MatchInformationViewData?, completionHandler: @escaping(_ progress: MatchStatus) -> Void) {
        if let status = matchListInformation?.matchStatus, status == .notStarted {
            completionHandler(.notStarted)
        }else {
            self.requestMatchBid()
            self.configureTimer()
        }
    }
    
    func configureTimer() {
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(requestMatchBid), userInfo: nil, repeats: true)
    }
    
    func invalidateTimer() {
        if let timer = self.timer {
            timer.invalidate()
        }
    }
    
    @objc private func requestMatchBid() {
        if Reachability.isConnectedToNetwork() {
            service.getMatchBid(completionHandler: { (matchBid, error) in
                if let matchBid = matchBid?.matchBids, !matchBid.isEmpty {
                    var matchDetailViewData = self.mapper(the: matchBid)
                    matchDetailViewData.matchBids.sort { (first,second) in
                        first.matchTime > second.matchTime
                    }
                    self.delegate?.takeMatchBid(matchDetailViewData)
                }else {
                    if let errorBid = error {
                        self.delegate?.error(errorBid)
                    }
                }
            })
        }else {
            var error = BaseError()
            error.title = "Erro de conexão!"
            error.message = "Por favor, verifique se seu aparelho está com o pacote de dados ligado ou conectado a uma rede WiFi."
            self.delegate?.error(error)
        }
    }
    
    private func mapper(the object: [MatchBids]) -> MatchDetailViewData {
        var matchDetailViewData = MatchDetailViewData()
        for item in object {
            var matchBidsItem = MatchBidsViewData()
            if let matchTime = item.matchTime, let bidDescription = item.bidDescription, let bidImportance = item.bidImportance {
                matchBidsItem.matchTime = matchTime
                matchBidsItem.bidDescription = bidDescription
                matchBidsItem.bidImportance = bidImportance
                
                matchDetailViewData.matchBids.append(matchBidsItem)
            }
        }
        return matchDetailViewData
    }
    
}
