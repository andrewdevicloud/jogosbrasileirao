//
//  MatchBidTableViewCell.swift
//  Brasileirao
//
//  Created by Andrew Castro on 23/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import UIKit

final class MatchBidTableViewCell: UITableViewCell {
    
    @IBOutlet var matchTime: UILabel!
    @IBOutlet var descriptionBid: UILabel!
    @IBOutlet var bidImportance: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(matchTime: Int, descriptionBid: String, bidImportance: String) {
        self.matchTime.text = String(matchTime)
        self.descriptionBid.text = descriptionBid
        self.bidImportance.layer.cornerRadius = self.bidImportance.frame.height / 2
        switch bidImportance {
        case "alta":
            self.bidImportance.backgroundColor = UIColor.red
        case "media":
            self.bidImportance.backgroundColor = UIColor.blue
        default:
            self.bidImportance.backgroundColor = UIColor.lightGray
        }
    }
    
}
