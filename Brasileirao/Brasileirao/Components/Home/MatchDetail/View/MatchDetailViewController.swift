//
//  MatchDetailViewController.swift
//  Brasileirao
//
//  Created by Andrew Castro on 23/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import UIKit
import Kingfisher

class MatchDetailViewController: UIViewController {
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private var homeTeamShield: UIImageView!
    @IBOutlet private var homeTeamName: UILabel!
    @IBOutlet private var scoreboard: UILabel!
    @IBOutlet private var matchTime: UILabel!
    @IBOutlet private var outTeamShield: UIImageView!
    @IBOutlet private var outTeamName: UILabel!
    @IBOutlet private var matchLocal: UILabel!
    @IBOutlet private var dateAndHour: UILabel!
    @IBOutlet private var tableView: UITableView!
    
    var matchListInformation: MatchInformationViewData?
    var matchDetailViewData: MatchDetailViewData?
    var progress: MatchStatus?
    
    let unavailableMatch = UnavailableViewController()
    let presenter = MatchDetailPresenter()
}

extension MatchDetailViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.configureDelegate()
        self.checkProgressMatch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configure()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.invalidateTimer()
    }
}

extension MatchDetailViewController {
    
    private func configureTableView() {
        self.tableView.estimatedRowHeight = 85
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(UINib(nibName: "MatchBidTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchBidTableViewCell")
    }
    
    private func configureDelegate() {
        self.presenter.delegate = self
        self.unavailableMatch.delegate = self
    }
    
    private func checkProgressMatch() {
        self.tableView.isHidden = true
        self.activityIndicator.isHidden = false
        self.presenter.checkProgressMatch(matchListInformation: matchListInformation, completionHandler: {
            progress in
            if progress == .notStarted {
                self.tableView.isHidden = false
                self.activityIndicator.isHidden = true
                self.progress = progress
            }
        })
    }
    
    private func configure() {
        self.navigationItem.title = "Partida"
        self.homeTeamName.text = self.matchListInformation?.homeTeam.name
        self.scoreboard.text = "\(self.matchListInformation?.homeTeam.goalsMadeMatch ?? "0") x \(self.matchListInformation?.outTeam.goalsMadeMatch ?? "0")"
        self.matchTime.text = ""
        self.outTeamName.text = self.matchListInformation?.outTeam.name
        self.matchLocal.text = self.matchListInformation?.local
        self.dateAndHour.text = "\(self.matchListInformation?.matchDate ?? "") - \(self.matchListInformation?.hour ?? "")"
        self.setImage(homeTeamShieldUrl: self.matchListInformation?.homeTeam.shield ?? "", outTeamShieldUrl: self.matchListInformation?.outTeam.shield ?? "")
    }
    
    private func setImage(homeTeamShieldUrl: String, outTeamShieldUrl: String) {
        let homeTeamShieldUrl = URL(string: homeTeamShieldUrl)
        self.homeTeamShield.kf.indicatorType = .activity
        self.homeTeamShield.kf.setImage(with: homeTeamShieldUrl, placeholder: UIImage(named: "emptyShield"))
        let outTeamShieldUrl = URL(string: outTeamShieldUrl)
        self.outTeamShield.kf.indicatorType = .activity
        self.outTeamShield.kf.setImage(with: outTeamShieldUrl, placeholder: UIImage(named: "emptyShield"))
    }
}

extension MatchDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if progress == .notStarted {
            self.tableView.backgroundView = unavailableMatch.view
            return 0
        }
        return self.matchDetailViewData?.matchBids.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "MatchBidTableViewCell", for: indexPath) as? MatchBidTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(matchTime: self.matchDetailViewData?.matchBids[indexPath.row].matchTime ?? 0, descriptionBid: self.matchDetailViewData?.matchBids[indexPath.row].bidDescription ?? "", bidImportance: self.matchDetailViewData?.matchBids[indexPath.row].bidImportance ?? "")
        return cell
    }
}

extension MatchDetailViewController: MatchDetailPresenterDelegate {
    
    func takeMatchBid(_ matchDetailViewData: MatchDetailViewData) {
        self.tableView.isHidden = false
        self.activityIndicator.isHidden = true
        self.matchDetailViewData = matchDetailViewData
        self.tableView.reloadData()
    }
    
    func error(_ error: BaseError) {
        self.tableView.isHidden = false
        self.activityIndicator.isHidden = true
        self.progress = .notStarted
        self.tableView.reloadData()
    }    
}

extension MatchDetailViewController: UnavailableViewControllerDelegate {
    func refreshViewController() {
        self.checkProgressMatch()
    }
}
