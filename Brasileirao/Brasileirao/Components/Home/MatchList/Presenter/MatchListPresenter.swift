//
//  MatchListPresenter.swift
//  Brasileirao
//
//  Created by Andrew Castro on 22/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation

protocol MatchListCacheProtocol {
    func sendMatchListCache(_ matchListCache: [MatchInformationUserDefaults])
}

enum MatchStatus: String {
    case notStarted = "Não começou"
    case firstTime = "1T"
    case interval = "Intervalo"
    case secondTime = "2T"
    case ended = "Terminou"
}

struct MatchListViewData {
    var round = 1
    var matchList = [MatchInformationViewData]()
}

struct MatchInformationViewData {
    var id = 1
    var homeTeam = TeamInformationViewData()
    var outTeam = TeamInformationViewData()
    var matchDate = ""
    var hour = ""
    var local = ""
    var matchStatus: MatchStatus?
}

struct TeamInformationViewData {
    var name = ""
    var shield = ""
    var goalsMadeMatch = ""
}

class MatchListPresenter {
    
    var delegate: MatchListCacheProtocol?
    
    private let userDefaultsManager = UserDefaultsManager()
    private let service = MatchListService("https://private-3b78a5-andrewcastro.apiary-mock.com/matchList")
    
    func callMatchList(completionHandler: @escaping(_ matchList: MatchListViewData?, _ error: BaseError?) -> Void) {
        if Reachability.isConnectedToNetwork() {
            self.service.getMatchList(completionHandler: { [unowned self] (result, error) in
                if let matchList = result {
                    let object = self.parse(the: matchList)
                    if let dataCache = self.userDefaultsManager.getSaveContacts(), dataCache.isEmpty {
                        self.userDefaultsManager.saveObject(matchList: object.matchList)
                    }
                    completionHandler(object, nil)
                }else {
                    completionHandler(nil, error)
                }
            })
        }else {
            if let dataCache = self.userDefaultsManager.getSaveContacts(), !dataCache.isEmpty {
                self.delegate?.sendMatchListCache(dataCache)
            }else {
                var error = BaseError()
                error.title = "Erro de conexão!"
                error.message = "Por favor, verifique se seu aparelho está com o pacote de dados ligado ou conectado a uma rede WiFi."
                completionHandler(nil, error)
            }
        }
    }
    
    private func parse(the object: MatchList) -> MatchListViewData {
        var matchListViewData = MatchListViewData()
        if let matchList = object.matchList, !matchList.isEmpty{
            for matchInformation in matchList {
                matchListViewData.round = object.round ?? 1
                let matchInformationItem = self.mapper(the: matchInformation)
                if !matchInformationItem.homeTeam.name.isEmpty, !matchInformationItem.outTeam.name.isEmpty {
                    matchListViewData.matchList.append(matchInformationItem)
                }
            }
        }
        return matchListViewData
    }
    
    private func mapper(the object: MatchInformation) -> MatchInformationViewData {
        var matchInformationItem = MatchInformationViewData()
        if let id = object.id, let homeTeam = object.homeTeam, let outTeam = object.outTeam, let matchDate = object.matchDate, let hour = object.hour, let local = object.local, let matchStatus = object.matchStatus {
            matchInformationItem.matchDate = matchDate
            matchInformationItem.hour = hour
            matchInformationItem.local = local
            
            switch matchStatus {
            case "Não começou":
                matchInformationItem.matchStatus = .notStarted
            case "1T":
                matchInformationItem.matchStatus = .firstTime
            case "Intervalo":
                matchInformationItem.matchStatus = .interval
            case "2T":
                matchInformationItem.matchStatus = .secondTime
            default:
                matchInformationItem.matchStatus = .ended
            }
            matchInformationItem.id = id
            matchInformationItem.homeTeam = self.mapper(the: homeTeam, matchInformationItem: matchInformationItem)
            matchInformationItem.outTeam =  self.mapper(the: outTeam, matchInformationItem: matchInformationItem)
        }
        return matchInformationItem
    }
    
    private func mapper(the object: TeamInformation, matchInformationItem: MatchInformationViewData) -> TeamInformationViewData {
        var teamInformationItem = TeamInformationViewData()
        teamInformationItem.name = object.name ?? ""
        teamInformationItem.shield = object.shield ?? ""
        if matchInformationItem.matchStatus == .notStarted {
            teamInformationItem.goalsMadeMatch = ""
        }else {
            teamInformationItem.goalsMadeMatch = String(object.goalsMadeMatch ?? 0)
        }
        return teamInformationItem
    }
}
