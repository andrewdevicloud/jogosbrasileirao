//
//  MatchListViewController.swift
//  Brasileirao
//
//  Created by Andrew Castro on 22/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import UIKit

enum ConditionViewController {
    case error
    case success
    case waiting
    case usingCache
}

class MatchListViewController: UIViewController {

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    let presenter = MatchListPresenter()
    let unavailableMatch = UnavailableViewController()
    
    var baseError: BaseError?
    var condition: ConditionViewController = .waiting
    var object: MatchListViewData?
    var matchListCache: [MatchInformationUserDefaults]?
}

extension MatchListViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.delegate = self
        self.unavailableMatch.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callMatchList()
        self.configureTableView()
    }
}

extension MatchListViewController {
    
    private func callMatchList() {
        self.configureLoading(isHidden: false)
        self.presenter.callMatchList(completionHandler: { [unowned self] (matchList, error) in
            self.configureLoading(isHidden: true)
            if let object = matchList {
                self.condition = .success
                self.object = object
            }else {
                self.baseError = error
                self.condition = .error
            }
            self.tableView.isHidden = false
            self.tableView.reloadData()
        })
    }
    
    private func configureTableView() {
        self.tableView.register(UINib(nibName: "MatchTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchTableViewCell")
        self.tableView.sectionHeaderHeight = 60
    }
    
    private func configureLoading(isHidden: Bool) {
        self.tableView.isHidden = true
        self.activityIndicator.isHidden = isHidden
        if isHidden == false {
            self.activityIndicator.startAnimating()
        }else {
            self.activityIndicator.stopAnimating()
        }
    }
    
    private func alert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Erro de conexão", message: "Por favor, verifique sua conexão! Para acessar a partida você precisa estar conectado a uma rede WiFi ou ter um pacote de dados.", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            let connected = UIAlertAction(title: "Estou conectado", style: .default, handler: { _ in
                self.callMatchList()
            })
            alert.addAction(connected)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension MatchListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.condition == .error {
            self.tableView.backgroundView = unavailableMatch.view
            self.unavailableMatch.error = self.baseError
        }else if self.condition == .usingCache {
            return self.matchListCache?.count ?? 0
        }
        return self.object?.matchList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "MatchTableViewCell", for: indexPath) as? MatchTableViewCell else {
            return UITableViewCell()
        }
        if self.condition == .usingCache {
            if let matchListCache = self.matchListCache?[indexPath.row] {
                cell.configureUsingCache(matchListCache)
            }
        }else {
            if let matchList = self.object?.matchList[indexPath.row] {
                cell.configure(matchList)
            }
        }
        return cell
    }
}

extension MatchListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = UIView()
        cell.backgroundColor = UIColor.white
        cell.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 18)
        if condition != .error {
            self.tableView.separatorStyle = .singleLine
            let textLabel = UILabel()
            textLabel.text = "Rodada " + String(self.object?.round ?? 1)
            textLabel.frame = CGRect(x: 20, y: 22, width: tableView.frame.width, height: 18)
            textLabel.font = UIFont.boldSystemFont(ofSize: 18)
            textLabel.textColor = UIColor.black
            cell.addSubview(textLabel)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = UIStoryboard(name: "MatchDetail", bundle: nil).instantiateViewController(withIdentifier: "MatchDetailViewController") as? MatchDetailViewController else {
            return
        }
        if self.condition != .usingCache {
            let matchListInformation = self.object?.matchList[indexPath.row]
            controller.matchListInformation = matchListInformation
            self.navigationController?.pushViewController(controller, animated: true)
        }else {
            self.alert()
        }
    }
}

extension MatchListViewController: MatchListCacheProtocol {
    
    func sendMatchListCache(_ matchListCache: [MatchInformationUserDefaults]) {
        self.condition = .usingCache
        self.matchListCache = matchListCache
        self.configureLoading(isHidden: true)
        self.tableView.isHidden = false
        self.tableView.reloadData()
    }
}

extension MatchListViewController: UnavailableViewControllerDelegate {
    func refreshViewController() {
        self.callMatchList()
    }
}
