//
//  MatchTableViewCell.swift
//  Brasileirao
//
//  Created by Andrew Castro on 22/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import UIKit
import Kingfisher

final class MatchTableViewCell: UITableViewCell {

    @IBOutlet private var dateAndTime: UILabel!
    @IBOutlet private var matchScore: UILabel!
    @IBOutlet private var homeTeamShield: UIImageView!
    @IBOutlet private var homeTeamName: UILabel!
    @IBOutlet private var outTeamShield: UIImageView!
    @IBOutlet private var outTeamName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ object: MatchInformationViewData) {
        self.dateAndTime.text = object.matchDate + " " + object.hour
        self.matchScore.text = object.homeTeam.goalsMadeMatch + " x " + object.outTeam.goalsMadeMatch
        self.homeTeamName.text = object.homeTeam.name
        self.outTeamName.text = object.outTeam.name
        self.setImage(homeTeamShieldUrl: object.homeTeam.shield, outTeamShieldUrl: object.outTeam.shield)
    }
    
    func configureUsingCache(_ object: MatchInformationUserDefaults) {
        self.dateAndTime.text = object.matchDate + " " + object.hour
        self.matchScore.text = object.homeTeam.goalsMadeMatch + " x " + object.outTeam.goalsMadeMatch
        self.homeTeamName.text = object.homeTeam.name
        self.outTeamName.text = object.outTeam.name
        self.setImage(homeTeamShieldUrl: object.homeTeam.shield, outTeamShieldUrl: object.outTeam.shield)
    }
    
    private func setImage(homeTeamShieldUrl: String, outTeamShieldUrl: String) {
        let homeTeamShieldUrl = URL(string: homeTeamShieldUrl)
        self.homeTeamShield.kf.indicatorType = .activity
        self.homeTeamShield.kf.setImage(with: homeTeamShieldUrl, placeholder: UIImage(named: "emptyShield"))
        
        let outTeamShieldUrl = URL(string: outTeamShieldUrl)
        self.outTeamShield.kf.indicatorType = .activity
        self.outTeamShield.kf.setImage(with: outTeamShieldUrl, placeholder: UIImage(named: "emptyShield"))
    }
}
