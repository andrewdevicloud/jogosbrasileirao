//
//  Reachability.swift
//  Brasileirao
//
//  Created by Andrew Castro on 23/08/19.
//  Copyright © 2019 Andrew Castro. All rights reserved.
//

import Foundation
import Alamofire
import SystemConfiguration

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
    
    class func checkTypeConnectionWWAN() -> Bool {
        
        var WWAN = false
        let net = NetworkReachabilityManager()
        let result = net?.isReachable
        
        net?.startListening()
        if (result != nil && result!) {
            if (net?.isReachableOnEthernetOrWiFi)! {
                WWAN = false
            } else if (net?.isReachableOnWWAN)! {
                WWAN = true
            }
        }
        return WWAN
    }
    
}
